var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 6000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
let myTime = 10000;

function getTime(input){
    readBooksPromise(myTime, books[input])
    .then(function (fullfilled){
        console.log(fullfilled);
        myTime = fullfilled;
        if(books.length!=input+1){
            getTime(input+1)
        }
    
    })
    .catch(function (error){
        console.log(error);
    });
}
getTime(0);