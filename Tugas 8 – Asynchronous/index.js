var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini
let myTime = 10000;
function timeCount(input){
    if(input == books.length){
        return 0
    }else{
        readBooks(myTime, books[input], function(time){
            myTime = time;
            timeCount(input+1)
        })
    }
}
timeCount(0)