import React, { Component } from 'react';
import { TextInput, StyleSheet, Text, View, Image, Button, TouchableOpacity, FlatList } from 'react-native';
import Icon from 'react-native-ionicons'
import { Entypo } from '@expo/vector-icons';

export default class App extends Component {

    state = {
        email: '',
        password: '',
        username: '',
        repassword: ''
     }
     handleEmail = (text) => {
        this.setState({ email: text })
     }
     handlePassword = (text) => {
        this.setState({ password: text })
     }
     handleUsername = (text) => {
        this.setState({ username: text })
     }
     handleRePassword = (text) => {
        this.setState({ repassword: text })
     }
    
    render() {
        return (
          <View style={styles.container}>
              <Image source={require('./images/logo.png')} style={styles.imageTop}/>
              <Text style={styles.textTitle}>HIRE ME APP</Text>
              <Text style={styles.textTitle}>REGISTER</Text>

              <Text style={[styles.textInput, {marginTop: 30}]}>Email</Text>
              <TextInput style = {styles.input}
               placeholder = "Email"
               onChangeText = {this.handleEmail}/>
               
               <Text style={styles.textInput}>Password</Text>
               <TextInput style = {styles.input}
                placeholder = "Password"
                onChangeText = {this.handlePassword}/>
                
                <Text style={styles.textInput}>Username</Text>
                <TextInput style = {styles.input}
                placeholder = "Username"
                onChangeText = {this.handleEmail}/>
                
                <Text style={styles.textInput}>Re-Password</Text>
                <TextInput style = {styles.input}
                placeholder = "Re-Password"
                onChangeText = {this.handlePassword}/>
                
                <TouchableOpacity style={styles.loginBtn}>
                    <Text style={styles.loginText}>REGISTER</Text>
                </TouchableOpacity>

                <Text style={styles.textRegister}>Login</Text>

          </View>
        );
      }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center'
    },
    imageTop: {
        flex: 1,
        width: 300,
        height: 100,
        marginTop: 20
    },
    textTitle:{
        flex: 1,
        fontSize: 20,
        fontWeight: "600",
        marginTop: 5
    },
    textInput:{
        fontSize: 16,
        fontWeight: "600",
        marginTop: 8,
        alignItems: 'flex-start',
        justifyContent: 'flex-start'
    },
    input: {
        height: 40,
        width:"70%",
        borderRadius: 6,
        paddingStart : 10,
        borderColor: '#0B50B8',
        borderWidth: 1,
        placeholderTextColor: "#C4C4C4",
        autoCapitalize : "none",
        underlineColorAndroid : "transparent"
     },
     loginText: {
        color: "#ffffff"
     },
     loginBtn:{
        width:"70%",
        backgroundColor:"#0B50B8",
        borderRadius:6,
        height:40,
        alignItems:"center",
        justifyContent:"center",
        marginTop:40,
        marginBottom:10
      },
      textRegister:{
          color: "#0B50B8",
          marginTop: 2,
          fontSize: 16
      }
  });