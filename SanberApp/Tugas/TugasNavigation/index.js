import React from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer'

// import { Login, About } from './Screen';
import Login from './LoginScreen'
import About from './AboutScreen'
import Skill from './SkillScreen'
import Project from './ProjectScreen'
import Add from './AddScreen'

const AuthStack = createStackNavigator();
const Tabs = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

const TabsScreen = () => (
  <Tabs.Navigator>
    <Tabs.Screen name="Skill" component={Skill}/>
    <Tabs.Screen name="Project" component={Project}/>
    <Tabs.Screen name="Add" component={Add}/>
  </Tabs.Navigator>
);

const DrawerScreen = () => (
  <Drawer.Navigator>
    <Drawer.Screen name='Skill' component={TabsScreen} />
    <Drawer.Screen name='About' component={About} />
  </Drawer.Navigator>
);

export default () => (
  <NavigationContainer>
    <AuthStack.Navigator>
      <AuthStack.Screen name="Login" component={Login} />
      <AuthStack.Screen name="Home" component={DrawerScreen} />
    </AuthStack.Navigator>
  </NavigationContainer>
)
