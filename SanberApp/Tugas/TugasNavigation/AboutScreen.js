import React, { Component } from 'react';
import { TextInput, StyleSheet, Text, View, Image, Button, TouchableOpacity, FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
export default class App extends Component {

    render() {
        return (
          <View style={styles.container}>
              <View style={styles.containerTop}>
                <MaterialCommunityIcons name="menu" size={40} color="#ffffff" onPress={() => this.props.navigation.toggleDrawer()}/>
                <Icon  name={'user-o'} size={70}style={styles.imageTop} />
                <Text style={styles.textName}>Dimas Oktanugraha</Text>
                <Text style={styles.textRole}>Developer</Text>
              </View>

              <View style={styles.containerData}>
                <Text style={styles.textNormal}>Portofolio</Text> 
                <View style={styles.rowIcon}>
                    <View style={styles.iconColumnStyle}>
                        <Icon  name={'gitlab'} size={40} style={{color:"#0B50B8"}} />
                        <Text style={styles.textStyle}>@dimas_oktanugraha</Text>
                    </View>
                    <View style={styles.iconColumnStyle }>
                        <Icon  name={'github'} size={40} style={{color:"#0B50B8"}}/>
                        <Text style={styles.textStyle}>@dimas_oktanugraha</Text>
                    </View>
                </View> 
              </View>

              <View style={styles.containerData}>
                <Text style={styles.textNormal}>Contact</Text> 
                <View style={styles.columnIcon}>
                    <View style={styles.iconRowStyle}>
                        <Icon  name={'instagram'} size={40} style={{color:"#0B50B8"}} />
                        <Text style={styles.textStyle}>@dimas_oktanugraha</Text>
                    </View>
                    <View style={styles.iconRowStyle }>
                        <Icon  name={'twitter'} size={40} style={{color:"#0B50B8"}}/>
                        <Text style={styles.textStyle}>@dimas_oktanugraha</Text>
                    </View>
                </View> 
              </View>

          </View>
        );
      }
}

const styles = StyleSheet.create({
    container: {
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: "#FFFFFF"
    },
    containerTop:{
        flex: 1,
        height: 200,
        alignItems: 'center',
        backgroundColor:"#0B50B8",
        justifyContent: 'center',
        width:"100%",
        borderBottomLeftRadius: 8,
        borderBottomRightRadius: 8
    },
    imageTop:{
        marginTop: 2,
        marginBottom: 5,
        color: "#ffffff"
    },
    textName:{
        fontSize: 20,
        fontWeight: "600",
        marginTop: 5,
        color: "#ffffff"
    },
    textRole:{
        fontSize: 16,
        fontWeight: "400",
        marginBottom: 20,
        color: "#ffffff"
    },
    containerData:{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 8,
        width: "80%", 
        marginTop: 40,
        padding: 10,
        backgroundColor: "#EFEFEF"
    },
    textNormal:{
        fontSize: 16,
        marginBottom: 20,
        color: "#000000"
    },
    iconColumnStyle: {
        color: "#0B50B8",
        flexDirection: 'column',
        alignItems: 'center',
        padding: 5,
    },
    iconRowStyle:{
        color: "#0B50B8",
        flexDirection: 'row',
        alignItems: 'center',
        padding: 5,
    },
    textStyle:{
        fontSize: 10,
        margin: 10
    },
    rowIcon:{
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center'
    },
    columnIcon:{
        flexDirection: 'column',
        justifyContent: 'space-evenly',
        alignItems: 'center'
    }
  });