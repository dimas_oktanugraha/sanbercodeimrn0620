import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity
} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'



export default class SkillItem extends Component {
    render() {
        let skill = this.props.skill;
        return (
            <View style={styles.container}>
                <MaterialCommunityIcons  name={skill.iconName} size={80} style={styles.icon} />
                <View style={styles.dataContainer}>
                    <Text style={styles.skillName}>{skill.skillName}</Text>
                    <Text style={styles.skillCategory}>{skill.categoryName}</Text>
                    <Text style={styles.skillProgress}>{skill.percentageProgress}</Text>
                </View>
                <MaterialCommunityIcons  name={'chevron-right'} size={80} style={styles.icon} />
                
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#B4E9FF",
      width: '100%',
      flexDirection: 'row',
      padding: 10,
      marginTop: 10,
      borderRadius: 6,
      elevation: 5,
      justifyContent: 'space-between',
      alignItems: 'center',
    },
    icon:{
        color: "#003366",
        alignItems: "center",
    },
    dataContainer:{
        flexDirection: 'column',
        alignItems:'stretch'
    },
    skillName:{
        color: "#003366",
        fontSize: 24,
        fontWeight: '600',
    },
    skillCategory:{
        color: "#000000",
        fontSize: 16
    },
    skillProgress:{
        color: "#ffffff",
        fontSize: 40,
        fontWeight: '600',
        textAlign: 'right'
    }
  });
