import React, { Component } from 'react';
import { TextInput, StyleSheet, Text, View, Image, Button, TouchableOpacity, FlatList } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import SkillItem from './components/SkillItem';
import data from './skillData.json';

export default class SkillScreen extends Component {

    render() {
        return (
          <View style={styles.container}>
              <View style={styles.viewTop}>
                <Image source={require('./images/logo.png')} style={styles.imageTop}/>
              </View>
              <View style={styles.nameContainter}>
                <MaterialCommunityIcons  name={'account-circle'} size={40}style={styles.imageUser} />
                <View style={styles.nameUser}>
                    <Text style={[styles.name, {fontSize: 12}]}>Hai,</Text>
                    <Text style={[styles.name, {fontSize: 14}]}>Dimas Oktanugraha</Text>
                </View>
              </View>
              <Text style={styles.skill}>SKILL</Text>
              <View style={styles.separator}></View>
              <View style={styles.categoryContainer}>
                <Text style={styles.category}>Library / Framework</Text>
                <Text style={styles.category}>Bahasa Pemrograman</Text>
                <Text style={styles.category}>Teknologi</Text>
              </View>

              <View style={styles.body}>
                <FlatList
                    data={data.items}
                    renderItem={(skill)=><SkillItem skill={skill.item} />}
                    keyExtractor={(item)=>item.id}
                    ItemSeparatorComponent={()=><View style={{height:0.5,backgroundColor:'#E5E5E5'}}/>}
                    />
                </View>
            </View>
        );
      }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      marginStart: 20,
      marginEnd: 20
    },
    viewTop:{
        flexDirection: 'row',
        justifyContent: 'flex-end',
        marginRight: 20
    },
    imageTop:{
        height: 60,
        width: 170,
    },
    nameContainter:{
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginTop: 10
    },
    imageUser:{
        color: "#B4E9FF",
    },
    nameUser:{
        marginStart:10,
        flexDirection: 'column',
        justifyContent: 'flex-start',
    },
    name:{
        color: "#003366",
        fontWeight: '600'
    },
    skill:{
        fontSize: 36,
        marginTop: 10,
        color: "#003366",
        fontWeight: '600'
    },
    separator:{
        height:5,
        width: '100%',
        backgroundColor:"#003366"
    },
    categoryContainer:{
        flexDirection: 'row',
        marginTop: 10,
        justifyContent: 'space-between'
    },
    category:{
        backgroundColor: "#B4E9FF",
        color: "#003366",
        padding: 5,
        borderRadius: 8,
        fontWeight: '600'
    },
    body: {
        flex: 1
      },
  });