//SOAL 1. Mengubah fungsi menjadi fungsi arrow
const golden = function goldenFunction(){
    console.log("this is golden!!")
}
   
golden()

//ES6
const golden2 = () => {
    console.log("this is golden!!")
}

golden2()


//SOAL 2. Sederhanakan menjadi Object literal di ES6
console.log("\n")

const newFunction = function literal(firstName, lastName){
    return {
        firstName: firstName,
        lastName: lastName,
        fullName: function(){
        console.log(firstName + " " + lastName)
        return 
        }
    }
}

newFunction("William", "Imoh").fullName() 

//ES6
const newFunction2 = (firstName, lastName) => {
    return{
        firstName,
        lastName,
        fullName: () => {
            console.log(`${firstName} ${lastName}`)
        }
    }    
}

newFunction2("William", "Imoh").fullName() 


//SOAL 3. Destructuring
console.log("\n")
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

// const firstName = newObject.firstName;
// const lastName = newObject.lastName;
// const destination = newObject.destination;
// const occupation = newObject.occupation;

// console.log(firstName, lastName, destination, occupation)

//ES6
const {firstName, lastName, destination, occupation, spell } = newObject
console.log(firstName, lastName, destination, occupation)


//SOAL 4. Array Spreading
console.log("\n")
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
// const combined = west.concat(east)
// console.log(combined)

//ES6
let combined = [...west, ...east]
console.log(combined)


//SOAL 5. Template Literals
console.log("\n")
const planet = "earth"
const view = "glass"
var before = 'Lorem ' + view + 'dolor sit amet, ' +  
    'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
    'incididunt ut labore et dolore magna aliqua. Ut enim' +
    ' ad minim veniam'
 
console.log(before) 
console.log("\n")

const after = `Lorem ${view}dolor sit amet, consectetur adipiscing elit,${planet}do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim  ad minim veniam`
console.log(after)