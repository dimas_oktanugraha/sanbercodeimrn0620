//soal 1
function teriak(){
    return "Halo Sanbers!";
}
console.log(teriak());

//soal 2
function kalikan(angka1, angka2){
    return angka1*angka2;
}

var num1 = 12;
var num2 = 4;
 
var hasilKali = kalikan(num1, num2);
console.log(hasilKali);

//soal 3
function introduce(textNama, textAge, textAddress, textHobby){
    return "Nama saya "+textNama+", umur saya "+textAge+" tahun, alamat saya di "+textAddress+", dan saya punya hobby yaitu "+textHobby+"!" 
}

var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan)