//Soal No. 1 (Array to Object)

function arrayToObject(arr){
    var now = new Date()
    var thisYear = now.getFullYear()

    for(var i = 0; i<arr.length; i++){
        var ageNum;
        if(arr[i].length<4 || thisYear < arr[i][3]){
            ageNum = "Invalid Birth Year"
        }else{
            ageNum = thisYear- arr[i][3];
        }

        var name = arr[i][0]+" "+arr[i][1];
        var objectArr = {
            firstName: arr[i][0],
            lastName: arr[i][1],
            gender: arr[i][2],
            age: ageNum
        }
        console.log(name+" : ");
        console.log(objectArr);
        console.log("\n");
    }
}

// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ];
arrayToObject(people) ;

var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ];
arrayToObject(people2) ;

// Error case 
arrayToObject([]);



//Soal No. 2 (Shopping Time)
function shoppingTime(memberId, money) {
    // you can only write your code here!
    if(memberId=="" || !memberId){
        return "Mohon maaf, toko X hanya berlaku untuk member saja";
    }else{
        if(money<50000){
            return "Mohon maaf, uang tidak cukup";
        }else{
            var outputShop = [""];
            var currentMoney = money;
            while(currentMoney>=50000){
                if(currentMoney>=1500000){
                    outputShop.push("Sepatu brand Stacattu")
                    currentMoney -= 1500000;
                }else if (currentMoney>=500000){
                    outputShop.push("Baju brand Zoro")
                    currentMoney -= 500000;
                }else if (currentMoney>=250000){
                    outputShop.push("Baju brand H&N")
                    currentMoney -= 250000;
                }else if (currentMoney>=175000){
                    outputShop.push("Sweater brand Uniklooh")
                    currentMoney -= 175000;
                }else if (currentMoney>=50000){
                    if(outputShop.includes("Casing Handphone")){
                        break;
                    }
                    outputShop.push("Casing Handphone")
                    currentMoney -= 50000;
                }
            }
            outputShop.shift();
            var output = {
                "memberId" : memberId,
                "money" : money,
                "listPurchased" : outputShop,
                "changeMoney": currentMoney
            }
            return output;
        }
    }
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); 
console.log(shoppingTime('234JdhweRxa53', 15000)); 
console.log(shoppingTime()); 


// Soal No. 3 (Naik Angkot)
function naikAngkot(arrPenumpang) {
    var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    //your code here
    if(arrPenumpang.length==0){
        return "[]"
    }else{
        var output = [""];
        for(var i = 0; i<arrPenumpang.length;i++){
            var bayar = 0;
            var start = rute.indexOf(arrPenumpang[i][1]);
            for(var b=start;b<(rute.length-start);b++){
                if(rute[b]!=arrPenumpang[i][2]){
                    bayar+=2000;
                }else{
                    break;
                }
            }
            var outputArr = {
                "penumpang" : arrPenumpang[i][0],
                "naikDari" : arrPenumpang[i][1],
                "tujuan" : arrPenumpang[i][2],
                "bayar" : bayar
            }
            output.push(outputArr);
        }
        output.shift();
        return output;
    }

  }
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]