//soal 1 (Range)
function range(startNum, finishNum){
    if(finishNum==null || startNum==null){
        return -1
    }else{
        var arrayData = [];
        if(startNum>finishNum){
            for(var i = startNum; i >= finishNum; i--){
                arrayData.push(i);
            }
        }else{
            for(var i = startNum; i <= finishNum; i++){
                arrayData.push(i);
            }
        }
        return arrayData;
    }
}

console.log(range(1, 10))
console.log(range(1))
console.log(range(11,18))
console.log(range(54, 50))
console.log(range())



//soal 2 (Range with Step)
console.log("\n");
function rangeWithStep(startNum, finishNum, step){
    var arrayData = [];
    if(startNum>finishNum){
        for(var i = startNum; i >= finishNum; i-=step){
            arrayData.push(i);
        }
    }else{
        for(var i = startNum; i <= finishNum; i+=step){
            arrayData.push(i);
        }
    }
    return arrayData;
}

console.log(rangeWithStep(1, 10, 2)) 
console.log(rangeWithStep(11, 23, 3)) 
console.log(rangeWithStep(5, 2, 1)) 
console.log(rangeWithStep(29, 2, 4)) 


//soal 3 (Sum of Range)
console.log("\n");
function sum(startNum = 0, finishNum, step = 1){
    if(finishNum!=null){
        var sumData = 0;
        if(startNum>finishNum){
            for(var i = startNum; i >= finishNum; i-=step){
                sumData+=i;
            }
        }else{
            for(var i = startNum; i <= finishNum; i+=step){
                sumData+=i;
            }
        }
        return sumData;
    }else{
        return startNum;
    }
    
}

console.log(sum(1,10));
console.log(sum(5, 50, 2));
console.log(sum(15,10));
console.log(sum(20, 10, 2));
console.log(sum(1));
console.log(sum());



var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 
var desc = ["Nomor ID:  ", "Nama Lengkap:  ", "TTL:  ", "Hobi:  "]

function dataHandling(arrayInput){
    for(var i = 0; i < arrayInput.length; i++){
        for(var j = 0; j < arrayInput[i].length;j++){
            console.log(desc[j]+arrayInput[i][j])
        }
        console.log("\n")
    }
}

dataHandling(input);

//soal 5 (Balik Kata)
console.log("\n");

function balikKata(input){
    var output = "";
    for(var i = input.length-1; i>=0; i--){
        output+=input[i];
    }
    return output;
}

console.log(balikKata("Kasur Rusak"))
console.log(balikKata("SanberCode"))
console.log(balikKata("Haji Ijah"))
console.log(balikKata("racecar"))
console.log(balikKata("I am Sanbers"))

//soal 6 (Metode Array)
console.log("\n");

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);

function dataHandling2(arrayInput){
    arrayInput.splice(1,1, arrayInput[1]+"Elsharawy")
    arrayInput.splice(2,1, "Provinsi "+arrayInput[2])
    arrayInput.splice(4,1,"Pria", "SMA Internasional Metro")
    console.log(arrayInput)

    var date = arrayInput[3].split("/")

    var month;
    switch(date[1]){
        case "01":
            month = "Januari";
            break;
        case "02":
            month = "Februari";
            break;
        case "03":
            month = "Maret";
            break;
        case "04":
            month = "April";
            break;
        case "05":
            month = "Mei";
            break;
        case "06":
            month = "Juni";
            break;
        case "07":
            month = "Juli";
            break;
        case "08":
            month = "Agustus";
            break;
        case "09":
            month = "September";
            break;
        case "10":
            month = "Oktober";
            break;
        case "11":
            month = "November";
            break;
        case "12":
            month = "Desember";
            break;
    }
    console.log(month);
    
    date.sort(function (value1, value2) { return  value2 - value1 } );
    console.log(date);

    var newData = date.join("-");
    console.log(newData);
    console.log(arrayInput[1].slice(0,14));
}