//tugas if else

var nama = "John"
var peran = "Guard"

if(nama==""){
    console.log("Nama harus diisi!")
}else{
    if(peran==""){
        console.log("Halo "+ nama+", Pilih peranmu untuk memulai game!")
    }else if(peran=='Penyihir'){
        console.log("Selamat datang di Dunia Werewolf, "+ nama)
        console.log("Halo Penyihir "+ nama+", kamu dapat melihat siapa yang menjadi werewolf!")
    }else if(peran=='Guard'){
        console.log("Selamat datang di Dunia Werewolf, "+ nama)
        console.log("Halo Guard "+ nama+", kamu akan membantu melindungi temanmu dari serangan werewolf.")
    }else if(peran=='Werewolf'){
        console.log("Selamat datang di Dunia Werewolf, "+ nama)
        console.log("Halo Werewolf "+ nama+", Kamu akan memakan mangsa setiap malam!")
    }
}

//Switch Case
var hari = 21; 
var bulan = 1; 
var tahun = 1945;

var month;

switch(bulan){
    case 1:
        month = ' Januari ';
        break;
    case 2:
        month = ' Februari ';
        break;
    case 3:
        month = ' Maret ';
        break;
    case 4:
        month = ' April ';
        break;
    case 5:
        month = ' Mei ';
        break;
    case 6:
        month = ' Juni ';
        break;
    case 7:
        month = ' Juli ';
        break;
    case 8:
        month = ' Agustus ';
        break;
    case 9:
        month = ' September ';
        break;
    case 10:
        month = ' Oktober ';
        break;
    case 11:
        month = ' November ';
        break;
    case 12:
        month = ' Desember ';
        break;
}

console.log(hari+month+tahun)