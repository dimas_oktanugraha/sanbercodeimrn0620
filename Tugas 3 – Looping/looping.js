//Tugas 1 Looping While

var dataLoop1 = 2;
var dataLoop2 = 20;

console.log("LOOPING PERTAMA")
while(dataLoop1<=20){
    console.log(dataLoop1+" - I love coding");
    dataLoop1+=2;
}

console.log("\nLOOPING KEDUA")
while(dataLoop2>=2){
    console.log(dataLoop2+" - I will become a mobile developer");
    dataLoop2-=2;
}

//Tugas 2 Looping menggunakan for
console.log("\nOUTPUT")
for(var i=1;i<=20;i++){
    if(i%2==0){
        console.log(i+ " - Berkualitas")
    }else if(i%3==0){
        console.log(i+ " - I Love Coding ")
    }else{
        console.log(i+ " - Santai")
    }
}

//Tugas 3 Membuat Persegi Panjang
console.log("\n")
for(var i=0;i<4;i++){
    var dataString = "";
    for(var j=0;j<8;j++){
        dataString +="#";
    }
    console.log(dataString)
}

//Tugas 4 Membuat Tangga
console.log("\n")
for(var i=1; i<=7; i++){
    console.log("#".repeat(i));
}

 //Tugas 5 Membuat Papan Catur
console.log("\n")
for(var i=0;i<8;i++){
    var dataString = "";
    for(var j=0;j<8;j++){
        if(i%2==0){
            if(j%2==0){
                dataString +=" ";
            }else{
                dataString +="#";
            }
        }else{
            if(j%2==0){
                dataString +="#";
            }else{
                dataString +=" ";
            }
        }
    }
    console.log(dataString)
}
